﻿(function (angular) {
	"use strict";
	angular.module('pi.ui.demo').controller('treeDemoController', [
			"$q", "$scope",
			function (q, scope) {
				var path = {
					"pikini-pikini": {
						"Grand person": true,
						"papa": true,
						"pikini": true
					},
					"my name is anything but Bart": {
						"Grand person": true,
						"simple person": true
					}
				};

				function promise(something) {
					var deferred = q.defer();
					deferred.resolve(something);
					return deferred.promise;
				}

				scope.tree = {
					root: {
						label: "Grand person",
						children: [
							{
								label: "papa",
								children: [
									{
										label: "pikini",
										children: [
											{
												label: "pikini-pikini",
												children: []
											}
										]
									},
									{
										label: "pikini-bro",
										children: []
									},
									{
										label: "pikini-sis",
										children: [
											{
												label: "pikini-coz",
												children: []
											}
										]
									}
								]
							},
							{
								label: "mama",
								children: [
									{
										label: "mikini",
										children: []
									}
								]
							},
							{
								label: "simple person",
								children: [
									{
										label: "my name is anything but Bart",
										children: []
									}
								]
							},
							{
								label: "an ordinary guy",
								children: []
							},
							{
								label: "an ordinary chic",
								children: []
							},
							{
								label: "a child who fell from heaven",
								children: []
							}
						]
					},
					toggleSelect: function (node, selected) {
						return !/coz/i.test(node.label);
					},
					preselectNode: function (node, trace) {
						return trace.parent && path["pikini-pikini"].hasOwnProperty(trace.parent.label);
					},
					preExpandBranch: function (node) {
						return path["pikini-pikini"].hasOwnProperty(node.label) || path["my name is anything but Bart"].hasOwnProperty(node.label);
					},
					labelFactory: function (node) {
						return promise(node.label);
					},
					actionFactory: function (node) {
						var actions = null;

						if (/coz/i.test(node.label)) {
							actions = [
								{
									name: "think",
									execute: function (node) {
										console.info("I thought of a node, here it is: ");
										console.dir(node);
									}
								}, {
									name: "remove",
									execute: function (node) {
										angular.noop();
									}
								}
							];
						} else {
							actions = [
								{
									name: "push",
									execute: function (node) {
										console.info("I pushed a node, here it is: ");
										console.dir(node);
									}
								},
								{
									name: "shove",
									execute: function (node) {
										console.info("I shoved a node, here it is: ");
										console.dir(node);
									}
								}
							];
						}

						return promise(actions);
					},
					childrenFactory: function (node) {
						return promise(node.children);
					},
					checkIfNodeHasChildren: function (node) {
						return promise(node.children && node.children.length);
					}
				};
			}
	]);
}(angular));