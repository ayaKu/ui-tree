const webpackConfig = require('./webpack.config.js');
webpackConfig.mode = 'development';

module.exports = config => {
	config.set({
		files: [ './test/unit/index.js' ],
		plugins: [
			'karma-webpack',
			'karma-sourcemap-loader',
			'karma-jasmine',
			'karma-chrome-launcher'
		],
		frameworks: [ 'jasmine' ],
		logLevel: config.LOG_INFO,
		browsers: [ 'Chrome', 'ChromeHeadless' ],
		customLaunchers: {
			ChromeHeadless: {
				base: 'Chrome',
				flags: [ '--headless', '--disable-gpu', '--disable-translate', '--disable-extensions', '--remote-debugging-port=9223', '--no-sandbox' ]
			},
			ChromeDebugging: {
				base: 'Chrome',
				flags: [ '--disable-translate', '--disable-extensions', '--remote-debugging-port=9333' ]
			}
		},
		preprocessors: {
			'./test/unit/index.js': [ 'webpack' ]
		},
		concurrency: Infinity,
		webpack: webpackConfig,

		webpackServer: { noInfo: true },

		port: 9976,

		webpackMiddleware: {
			stats: 'errors-only'
		}
	});
};
