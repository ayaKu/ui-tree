~function (angular) {
	"use strict";

	var tree = angular.module("pi.ui.tree", ["pi.ui.core"]);
	var singleselectToggle = function (node, self, trace) {
		self.activeSelections.forEach(function (item) { item.state.selected = false; });
		self.activeSelections = [];

		var currentSelection = {
			node: node,
			state: node.state
		};
		currentSelection.state.selected = !currentSelection.state.selected
		self.activeSelections.push(currentSelection);

		self.lastSelectedNode = node;
		self.lastSelectedNodeParent = trace;
		return currentSelection.state.selected;
	};

	// tree container - the root will be dropped here
	tree.directive("piUiTree", [
		"appRoot", "$timeout",
		function (appRoot, $timeout) {
			var scope = {
				root: "=",
				handle: "<",
				eagerLoad: "=?",
				multiselect: "<",
				toggleSelect: "&",
				preselectNode: "&",
				labelFactory: "&",
				actionFactory: "&",
				childrenFactory: "&",
				nodeIconFactory: "&",
				preExpandBranch: "&",
				generateIdForNode: "&",
				checkIfNodeHasChildren: "&",
				readonly: "<"
			};

			/**
			 * Tree controller constructor.
			 * @param {Object} $scope Scope.
			 * @param {Object} $element Element.
			 * @param {Object} $attrs Attributes.
			 * @returns {_L8.controller}
			 */
			var controller = function ($scope, $element, $attrs, $q, $interval, $anchorScroll) {
				var self = this;

				$scope.display = true;

				/**
				 * Given a handle object, the tree will add a method to it that can be used for
				 * refreshing/reloading the tree. Refreshing the tree will cause states like opened branch,
				 * selected nodes to be reevaluated and updated.
				 *
				 * For now this is just a refresh handle, but in the future, APIs for achieving other aims may be added to it.
				 */
				if (!($attrs.hasOwnProperty("handle") && angular.isObject($scope.handle))) {
					$scope.handle = {};
				}
				this.handle = $scope.handle;
				this.handle.loadingCounter = 0;
				this.handle.isLoaded = false;
				this.handle.childsLoaded = { count: 0};

				// Implementation of refresh function.
				this.handle.refresh = function (node) {
					this.childsLoaded.count = 0;
					$scope.display = false;
					$timeout(function () {
						if(node){
							$scope.updateSelectedNode = node;
						}
						$scope.display = true;
					});
				};
				// childrenLoading
				this.handle.numberOfNodesLeftToLoad = 0;


				/**
				 * True if children of nodes should be eager loaded, false otherwise.
				 * @type Boolean
				 */
				this.eagerLoad = $attrs.hasOwnProperty("eagerLoad");

				/**
				 * True if multiselect should be enabled, false otherwise.
				 * @type Boolean
				 */
				this.multiselect = $attrs.hasOwnProperty("multiselect");

				this.activeSelections = [];

				this.lastSelectedNode = undefined;
				this.lastSelectedNodeParent = undefined;

				this.networkElementArray = [];
				this.tempLastSelectedNode = null;
				this.tempSelectedNode = null;
				var multiselectToggle = function (node, shiftKeyPressed, trace) {

					if (shiftKeyPressed) {

						if (trace != null && trace.parent.model != null) {
							multiselectToggle(trace.model, true, trace.parent);
						}

						var siblings = node.getSiblings();
						if (self.networkElementArray.length == 0) {
							siblings.forEach(function (element) {
								self.networkElementArray.push(element);
							});
						}
						else {
							var indexOfBeginning = self.networkElementArray.indexOf(trace.model) + 1;
							if (indexOfBeginning != 0) {
								siblings.forEach(function (obj) {
									if (self.networkElementArray.indexOf(obj) == -1) {
										self.networkElementArray.splice(indexOfBeginning, 0, obj);
									}
									indexOfBeginning++;
								});
							}
						}


					}
					else {

						self.activeSelections.push({ state: node.state });
						node.state.selected = !node.state.selected;

						self.lastSelectedNode = node;
						self.lastSelectedNodeParent = trace;
					}

				};

				this.generateIdForNode = function (trackNodes) {
					return trackNodes ?
							function (node, trace) {
								return $scope.generateIdForNode({ node: node, trace: trace });
							} :
							function () {
								return {
									then: function (success) {
										success();
									}
								};
							};
				}($attrs.hasOwnProperty("generateIdForNode"));

				this.toggleSelect = function (multiselect) {
					return multiselect ?
							function (node, currentstate, trace, event, chil) {
								var selectionRejected = !$scope.toggleSelect({
									node: node,
									selected: currentstate.selected,
									multiSelectEnabled: event ? event.ctrlKey : true,
									shiftKeyPressed: event ? event.shiftKey : false,
									trace: trace,

								});

								if (selectionRejected) {
									return;
								}

								if (event) {
									if (event.ctrlKey) {
										multiselectToggle(node, event ? event.shiftKey : false, trace);
									}
									else if (event.shiftKey) {
										this.networkElementArray = [];
										if (self.lastSelectedNode != null) {
											multiselectToggle(node, event ? event.shiftKey : false, trace);
											multiselectToggle(self.lastSelectedNode, event ? event.shiftKey : false, self.lastSelectedNodeParent);

											var indexOfNode = self.networkElementArray.indexOf(node);
											var indexOfLastSelectedNode = self.networkElementArray.indexOf(self.lastSelectedNode);
											if (indexOfNode <= indexOfLastSelectedNode) {
												self.networkElementArray = self.networkElementArray.slice(indexOfNode, indexOfLastSelectedNode);
											}
											else {
												self.networkElementArray = self.networkElementArray.slice(indexOfLastSelectedNode + 1, indexOfNode + 1);
											}

											if (self.networkElementArray.length > 0) {
												var currentSelection = {
													node: self.networkElementArray[0],
													state: self.networkElementArray[0].state
												};
												currentSelection.state.selected = !currentSelection.state.selected
												self.activeSelections.push(currentSelection);
											}

											self.networkElementArray.shift();
											self.networkElementArray.forEach(function (item) {
												self.activeSelections.push({ state: item.state });
												item.state.selected = !item.state.selected;
											});

											self.lastSelectedNode = node;
											self.lastSelectedNodeParent = trace;
										}
									}
									else {
										singleselectToggle(node, self, trace);
									}
								} else { // for backward compatibility
									multiselectToggle(node, event ? event.shiftKey : false, trace);
								}
						} : function (node, currentstate, trace, event) {
							$scope.toggleSelect({
								node: node, selected: currentstate.selected, trace: trace
							}) && singleselectToggle(node, self, trace);
						};
				}(this.multiselect);

				this.preselectNode = function (node, state, trace) {
					var doSelect = $scope.preselectNode({
						node: node, trace: trace
					});

					if (doSelect) {
						self.toggleSelect(node, state, trace);
					}

					return doSelect;
				};

				this.preExpandNodeBranch = function (preExpandBranch) {
					return preExpandBranch ? function (node, trace) {
						return $scope.preExpandBranch({
							node: node, trace: trace
						});
					} : angular.noop;
				}($attrs.hasOwnProperty("preExpandBranch"));

				this.getIconForNode = function () {
					var _default = !$attrs.hasOwnProperty("nodeIconFactory"), factory;

					if (_default) {
						factory = function (state) {
							switch (state) {
								case "open":
									return "fa-caret-down";
								case "close":
									return "fa-caret-right";
								case "leaf":
									return "";
							}

							var msg = "Invalid state: expected `open` or `close` or `leaf`, found `" + state + "`. ";
							throw new Error(msg);
							console.trace();
						};
					} else {
						factory = function (state, node, trace) {
							return $scope.nodeIconFactory({
								node: node, trace: trace, state: state
							});
						};
					}

					return factory;
				}();

				this.getLabelForNode = function (node, trace) {
					return $scope.labelFactory({ node: node, trace: trace });
				};

				this.getActionsForNode = function (node, trace) {
					return $scope.actionFactory({ node: node, trace: trace });
				};

				this.checkIfNodeHasChildren = function (node, trace) {
					return $scope.checkIfNodeHasChildren({ node: node, trace: trace });
				};

				this.getChildrenForNode = function (node, trace) {
					return $scope.childrenFactory({ node: node, trace: trace });
				};

				$scope.$on("treeLoaded", function(){
					$scope.selectNode = $scope.updateSelectedNode;
				})
			};

			return {
				scope: scope,
				replace: true,
				restrict: "E",
				controller: controller,
				templateUrl: appRoot.resolve("module/pi.ui/tree/tree.html")
			};
		}
	]);

	// tree node - this is internal,
	// directive user should never reference this dirtectly
	tree.directive("piUiTreeNode", [
		"$compile",
		"appRoot",
		"$timeout",
		"$location",
		"$anchorScroll",
		function ($compile, appRoot, $timeout, $location, $anchorScroll) {

			var compile = function (element) {

				var contents = element.contents().remove(), compiledContents;

				/**
				 * Linking function.
				 * @param {Object} scope Scope.
				 * @param {Object} element Element.
				 * @param {Object} _ Attribute.
				 * @param {_L8.controller} tree Tree controller instance.
				 * @returns {undefined}
				 */
				var link = function (scope, element, _, tree) {
					var leafIcon = tree.getIconForNode("leaf", scope.node, scope.trace);
					var openIcon = tree.getIconForNode("open", scope.node, scope.trace);
					var closeIcon = tree.getIconForNode("close", scope.node, scope.trace);
					var spinnerIcon = "fa-spinner fa-spin";
					if (_.node == 'root') {
						tree.handle.loadingCounter = 1;
						tree.handle.isLoaded = false;
					}
					var updateIcons = function () {
						scope.nodeIconStyle[spinnerIcon] = scope.state.busy;
						scope.nodeIconStyle[openIcon] = scope.state.open && !scope.state.busy;
						scope.nodeIconStyle[closeIcon] = !scope.state.open && !scope.state.busy;
					};

					var loadChildren = function () {
						var dummyOffset = 1000;
						tree.handle.loadingCounter += dummyOffset;
						scope.state.busy = true;
						updateIcons();
						tree.getChildrenForNode(scope.node, scope.trace).then(function (children) {
							tree.handle.loadingCounter += children.length;
							scope.childsLoaded.count += children.length;
							children.forEach(function (item) {
								item.getSiblings = function (){
									return this.children
								}.bind(scope);
							});
							scope.children = children;
						}, function () {
							console.error("Failed to load children node: ");
							tree.handle.numberOfNodesLeftToLoad = 0;
							console.dir(scope.node);
						}).finally(function () {
							tree.handle.loadingCounter -= dummyOffset;
							if (tree.handle.loadingCounter == 0) {
								tree.handle.isLoaded = true;
							}
							scope.state.busy = false;
							tree.handle.numberOfNodesLeftToLoad--;
							--scope.childsLoaded.count;
							if(scope.childsLoaded.count == -1) {
								scope.$root.$broadcast("treeLoaded");
							}
							updateIcons();
						});
					};

					var scheduledToggleSelection;
					var cancelScheduledToggleSelection = function () {
						if (scheduledToggleSelection) {
							$timeout.cancel(scheduledToggleSelection);
							scheduledToggleSelection = null;
						}
					};

					scope.label = "...";
					scope.remove = false;
					scope.tracker = undefined;
					scope.nodeIconStyle = {};
					scope.showActions = true;

					scope.contextmenu = {
						isOpen: false
					};
					scope.execute = function (action) {
						if (action.keepMultipleDeletion === false) {
							singleselectToggle(scope.node, scope.node.state, tree);
						}
						var promise = action.execute(scope.node);
						if (promise) {
							promise.then(function finallyCallback() {
								if (action.name.toLowerCase() === "delete" || action.name.toLowerCase() === "remove")
									scope.removed = true;
							})
						}
						scope.contextmenu.isOpen = false;
					};

					scope.state = {
						busy: false,
						open: false,
						selected: undefined
					};
					scope.node.state = scope.state;

					scope.state.selected = tree.preselectNode(scope.node, scope.state, scope.trace);

					scope.toggleOpenClose = function ($event) {
						if ($event) {
							$event.preventDefault();
						}

						scope.state.open = !scope.state.open;
						updateIcons();
						if (scope.state.open) {
							scope.children || loadChildren();
						}
					};

					scope.toggleSelect = function ($event) {
						if ($event) {
							if ($event.shiftKey)
							{
								document.getSelection().removeAllRanges();
							}
							if (!scope.node.IsSelectable) {
								$event.stopPropagation();
								return;
							}
							$event.preventDefault();
							if (scope.readonly) {
								return;
							}
						}

						if (scheduledToggleSelection) {
							cancelScheduledToggleSelection();
							return;
						}

						scheduledToggleSelection = $timeout(function () {
							cancelScheduledToggleSelection();
							tree.toggleSelect(scope.node, scope.state, scope.trace, $event, scope.children);
						}, 300);
					};

					tree.getLabelForNode(scope.node, scope.trace).then(function (label) {
						scope.label = label;
					}, function () {
						console.info("Failed to create label for node: ");
						console.dir(scope.node);

						scope.label = "x| error trying to create name for node.";
					});

					tree.getActionsForNode(scope.node, scope.trace).then(function (actions) {
						if (actions && actions.length > 0) {
							scope.actions = actions;
							scope.showActions = true;
						}
						else {
							scope.showActions = false;
						}
					}, function () {
						console.info("Failed to create actions for node: ");
						console.dir(scope.node);

						scope.actions = [];
						scope.showActions = false;
					});

					tree.checkIfNodeHasChildren(scope.node, scope.trace).then(function (nodeHasChildren) {

						if (nodeHasChildren) {
							updateIcons();
							return;
						}

						scope.nodeIconStyle[leafIcon] = true;
						// override click behaviour
						scope.toggleOpenClose = angular.noop;
					});

					tree.generateIdForNode(scope.node, scope.trace).then(function (id) {
						scope.tracker = id;

						if (scope.state.selected) {
							// waiting for id to be applied
							$timeout(() => {
								$anchorScroll(id);
							})
						}
					}, function () {
						console.info("Failed to create id for node: ");
						console.dir(scope.node);
					});

					tree.eagerLoad && loadChildren();
					if(tree.preExpandNodeBranch(scope.node, scope.trace)) {
						scope.toggleOpenClose();
					} else {
						--scope.childsLoaded.count;
						if(scope.childsLoaded.count == -1) {
							scope.$root.$broadcast("treeLoaded");
						}
					}

					if (!compiledContents) {
						compiledContents = $compile(contents);
					}
					compiledContents(scope, function (clone) {
						element.append(clone);
					});
					tree.handle.loadingCounter--;
					if (tree.handle.loadingCounter == 0) {
						tree.handle.isLoaded = true;
					}
				};

				return link;
			};

			var controller = function ($scope) {
				$scope.$watch('selectNode', function () {
					if ($scope.selectNode && $scope.selectNode.nodeId
						&& $scope.selectNode.nodeId == $scope.tracker) {
						$scope.toggleSelect();
						if ($location.hash() !== $scope.selectNode.nodeId) {
							$location.hash($scope.selectNode.nodeId);
						}
					}
				})
			}

			return {
				scope: {
					node: "=",
					trace: "=",
					readonly: "=",
					childsLoaded: "=",
					selectNode: "="
				},
				replace: true,
				require: "^piUiTree",
				compile: compile,
				controller: controller,
				restrict: "EA",
				templateUrl: appRoot.resolve("module/pi.ui/tree/tree.node.html")
			};
		}]);
}(angular);
